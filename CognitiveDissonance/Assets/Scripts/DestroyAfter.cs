﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour {

    private float beginningOfExistanceAt;
    public float timeToExist = 5.0f;
    public bool randTime;

	// Use this for initialization
	void Start () {
        beginningOfExistanceAt = Time.time;

        if (randTime) {
            timeToExist = Random.Range(0.1f, 0.5f);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time - beginningOfExistanceAt > timeToExist) {
            Destroy(gameObject);
        }
		
	}
}
