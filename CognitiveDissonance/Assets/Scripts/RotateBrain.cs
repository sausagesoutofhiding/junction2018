﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBrain : MonoBehaviour {

	public int speed;
	public int removeSpeedEverySecond;

	// Use this for initialization
	void Start () {
		StartCoroutine (slowDown());
	}

	void Update() {
		transform.Rotate(Vector3.forward * speed * Time.deltaTime);
	}
	
	// Update is called once per frame
	public IEnumerator slowDown () {
		if (speed >= 0) 
		{
		yield return new WaitForSeconds (1);
			speed = speed - removeSpeedEverySecond;

			if (speed < 0) {
				speed = 0;
			}

			StartCoroutine (slowDown());
	}
	}
}