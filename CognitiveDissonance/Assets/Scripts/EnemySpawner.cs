﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject player;
	public GameObject enemy;
	public float spawnCooldown = 3.0f;
	public int enemySpeed = 10;
	public int chanceOfSpawn = 30;

	void Start() {
		StartCoroutine (Spawner ());
	}


	IEnumerator Spawner() {
		Spawn ();
		yield return new WaitForSeconds (spawnCooldown);
		StartCoroutine (Spawner ());
	}

	
	// Update is called once per frame
	void Spawn () {
		int spawnChance = Random.Range (0, 100);

		if (spawnChance <= chanceOfSpawn && player)
		{
			Vector2 target = player.transform.position;
			Vector2 myPos = this.transform.position;
			Vector2 direction = target - myPos;
			direction.Normalize();
			Quaternion rotation = Quaternion.Euler( 0, 0, Mathf.Atan2 ( direction.y, direction.x ) * Mathf.Rad2Deg );
			GameObject newEnemy = Instantiate (enemy, this.gameObject.transform);
			newEnemy.GetComponent<Rigidbody2D>().velocity = direction * enemySpeed;
		}
		}
}
