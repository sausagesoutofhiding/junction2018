﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooter : MonoBehaviour {

    public GameObject bullet;
	public Transform nozzle;
    public float bulletCooldown = 0.5f;
    public float bulletSpeed = 300.0f;
    private PlayerController playerController;
    private ShieldController shieldController;
    public ShootingEffect shootingEffect;
	public List<GameObject> fireAudioClips;

    private Color[] colorArray = {
        new Color(0.0f, 1.0f, 0.0f),
        new Color(0.5f, 0.4f, 0.5f),
        new Color(0.0f, 1.0f, 0.0f),
        new Color(0.8f, 1.0f, 0.8f),
        new Color(0.1f, 0.6f, 0.3f),
        new Color(0.4f, 0.2f, 0.7f),
        new Color(0.9f, 0.4f, 0.5f),
        new Color(0.1f, 0.8f, 0.9f)
    };

    public Transform canvas;

    private float lastShotAt = 0;

	// Use this for initialization
	void Start () {

        playerController = GetComponent<PlayerController>();
        shieldController = GetComponent<ShieldController>();


    }

	public void PlayClip() {
	
		if (fireAudioClips.Count != 0) {
			int audio = Random.Range (0, fireAudioClips.Count);
			this.GetComponent<AudioSource> ().clip = fireAudioClips [audio].GetComponent<AudioSource> ().clip;
			GetComponent<AudioSource> ().Play ();
		}

	}
	
	// Update is called once per frame
	void Update () {

        if (
            Input.GetMouseButton(0) &&
            Time.time - lastShotAt > bulletCooldown

        ) {
            Fire();
        }
	}

    private void Fire()
    {
        if (shieldController.GetIsShield()) {
            return;
        }


		PlayClip ();
        lastShotAt = Time.time;


        Vector3 target = Input.mousePosition;
        Vector3 currentPosition = transform.position;


        GameObject instantiatedBullet = Instantiate(bullet, new Vector2(nozzle.transform.position.x, nozzle.transform.position.y), Quaternion.identity);
        instantiatedBullet.transform.SetParent(canvas);

        instantiatedBullet.GetComponent<Image>().color = colorArray[Random.Range(0, 7)];


        Vector3 newDir = target - currentPosition;

        newDir.Normalize();


        shootingEffect.Trigger(newDir, bulletSpeed);

        instantiatedBullet.GetComponent<Rigidbody2D>().velocity = newDir * bulletSpeed;

    }
}
