﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
  
public class MainMenuHandler : MonoBehaviour
{
	[Tooltip ("Scene ID that will be loaded when ExitGame called with false parameter.")]
	public int MainMenuScene;

	[Tooltip ("Scene ID that will be the start of the game.")]
	public int FirstScene;

	[Tooltip ("Tag that is used to find in-game menu in every scene.")]
	public string InGameMenuTag;

	[Tooltip ("Bool that determines if in-game menu can be opened")]
	public bool CanOpenInGameMenu = false;

	[Tooltip ("Image used for fade in and out")]
	public Image fadeImage;

	[Tooltip ("Image color used for fading in and out")]
	public Color faderImageColor;

	[Tooltip ("Audio volume control slider")]
	public Slider volumeSlider;

	private bool MenuOpen = false;

	private Canvas InGameMenu;


	// Public void to set menu openable or not
	public void SetMenuOpenable(bool open) 
	{
		if (open) 
		{
			CanOpenInGameMenu = true;
		}
		else 
		{
			CanOpenInGameMenu = false;
		}
	}

	// At awake: timescale is reset, volumeslider value is set, fade in procedure is started and in-game menu object is found
	public void Awake() 
	{
		volumeSlider.value = AudioListener.volume;

		if (fadeImage != null) 
		{
			fadeImage.color = new Color (fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 1);
		}

		StartCoroutine( FadeInOrOut (true, false, 0));

		if (!string.IsNullOrEmpty(InGameMenuTag)) 
		{
			InGameMenu = GameObject.FindGameObjectWithTag (InGameMenuTag).GetComponent<Canvas> ();
		}
	}
		
	// Open menu or close it in other scenes except Main Menu Scene when Esc is pressed.
	public void Update() {
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{

			if (SceneManager.GetActiveScene ().buildIndex == MainMenuScene) 
			{
				ExitGame (true);
			} 
			else 
			{
				if (MenuOpen) 
				{
					ContinueGame ();
				} 
				else 
				{
					OpenMenu ();
				}
			}
		}
	}
		
	// Public void closes in-game menu
	public void ContinueGame()
	{
		if (MenuOpen) 
		{
			MenuOpen = false;
			Time.timeScale = 1;
			InGameMenu.enabled = false;
		}
	}

	// Public void opens in-game menu
	public void OpenMenu() 
	{
		if (CanOpenInGameMenu)
		{
			if (!MenuOpen) 
			{
				MenuOpen = true;
				Time.timeScale = 0;
				InGameMenu.enabled = true;
				GameObject[] texts = GameObject.FindGameObjectsWithTag ("Text");
				foreach (GameObject text in texts) {
					text.SetActive (false);
					text.SetActive (true);
				}
			}
		}
	}

	//Public IEnumerator for fade out and fade in effects
	IEnumerator FadeInOrOut(bool fadeIn, bool loadScene, int sceneID)
	{
		if (fadeImage != null) 
		{
			if (fadeIn) 
			{
				for (float i = 1; i >= 0; i -= Time.deltaTime) {
					fadeImage.color = new Color (fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, i);
					yield return null;
				}
			}
			else 
			{
				for (float i = 0; i <= 1; i += Time.deltaTime) {
					fadeImage.color = new Color (fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, i);
					yield return null;
				}
			}
		}

		if (loadScene) 
		{
			SceneManager.LoadScene (sceneID);
		} 
	}

		
	// Public void starts first scene from main menu.
	public void StartGame()
	{
		StartCoroutine(FadeInOrOut (false, true, FirstScene));
	}
		
	// Public void to exit game. Can exit completely or load main menu scene.
	public void ExitGame (bool quitCompletely)
	{
		Time.timeScale = 1;

		if (quitCompletely) 
		{
			Application.Quit ();
		} 
		else 
		{
			StartCoroutine(FadeInOrOut (false, true, MainMenuScene));
		}
	}

	// public void for loading a new scene. Scene ID passed as a parameter.
	public void LoadScene (int sceneID) 
	{
		StartCoroutine(FadeInOrOut (false, true, sceneID));
	}

	// Public void for setting game volume
	// Works now - was due editor issue not code:)
	public void SetGameVolume () 
	{
		AudioListener.volume = volumeSlider.value;
	}
}