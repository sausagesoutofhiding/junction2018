﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiPlayer : MonoBehaviour {

	public int numberOfDamageSpawns = 4;
	public GameObject enemy;
	public int enemySpeed;
	public GameObject GameCanvas;

	void OnTriggerEnter2D(Collider2D col)
	{
		
		if (col.gameObject.tag == "PlayerBullet") {

			GetComponent<AudioSource> ().Play ();
			Destroy (col.gameObject);

			for (int i = 0; i < numberOfDamageSpawns; i++) {

				GameObject newEnemy = Instantiate (enemy, transform.position, transform.rotation, GameCanvas.transform);
				newEnemy.GetComponent<Rigidbody2D>().velocity = Random.insideUnitSphere * enemySpeed;

			}
		}
	}
		

}
