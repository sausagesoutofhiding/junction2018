﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    Rigidbody2D body;
    float horizontal;
    float vertical;
    float moveLimiter = 0.7f;
    public float runSpeed = 1;
    private int lives = 3;
    private GameObject nozzle;  


    public void RemoveLife() {
        lives = lives - 1;
        nozzle.GetComponent<RectTransform>().localScale = new Vector3((float)lives / 3, (float)lives / 3, (float)lives / 3);
    }

    public int GetLives() {
        return lives;
    }

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        nozzle = GameObject.FindWithTag("Nozzle");
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

    }

    void FixedUpdate()
    {
        if (horizontal != 0 && vertical != 0) 
        {
            //body.velocity = new Vector2((horizontal * runSpeed) * moveLimiter, (vertical * runSpeed) * moveLimiter);

            body.AddForce(new Vector2((horizontal * runSpeed) * moveLimiter, (vertical * runSpeed) * moveLimiter));

        }
        else
        {
            body.AddForce(new Vector2(horizontal * runSpeed, vertical * runSpeed));

        }
    }
}
