﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LerpColor : MonoBehaviour {

	Color lerpedColor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		lerpedColor = Color.Lerp (Color.magenta, Color.cyan, Mathf.PingPong(Time.time, 2));
		GetComponent<Image> ().color = lerpedColor;
	}
}
