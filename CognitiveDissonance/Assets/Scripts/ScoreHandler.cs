﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour {

	public int currentScore = 0;
	public Text currentScoreText;
	public Text highScoreText;

	void Start() {

        initializeSave();

		if (highScoreText != null) {
            highScoreText.text = "High Score: " + GetHighScore().ToString();
		}
	}

    void initializeSave(){
        int? highScore = PlayerPrefs.GetInt("HighScore");

        if (highScore == null) {
            PlayerPrefs.SetInt("HighScore", 0);
        }
    }

	public void SetHighScore () {
		int highScore = GetHighScore ();

		if (highScore < currentScore) {
			PlayerPrefs.SetInt("HighScore", currentScore);
		}
	}

	public int GetHighScore () {
		int highScore = PlayerPrefs.GetInt("HighScore");

        return highScore;
    }

    public int GetScore()
    {
        return currentScore;
    }

    public void AddToScore (int points) {

		currentScore = currentScore + points;

		if (currentScoreText != null) {
			currentScoreText.text = "Score: " + currentScore.ToString ();
		}
	}
}
