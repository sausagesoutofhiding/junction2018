﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizePingPong : MonoBehaviour {

	public Vector3 mMaxScale;
	public Vector3 mMinScale;
	public Vector3 currentScaleSpeed;
	
	// Update is called once per frame
	void Update () {
		if (transform.localScale.x <= mMinScale.x || transform.localScale.x >= mMaxScale.x)
		{
			currentScaleSpeed.x *= -1; //invert it
		}

		if (transform.localScale.y <= mMinScale.y || transform.localScale.y >= mMaxScale.y)
		{
			currentScaleSpeed.y *= -1; //invert it
		}

		if (transform.localScale.z <= mMinScale.z || transform.localScale.z >= mMaxScale.z)
		{
			currentScaleSpeed.z *= -1; //invert it
		}

		transform.localScale = transform.localScale + currentScaleSpeed * Time.deltaTime;
	}
}
