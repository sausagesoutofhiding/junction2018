﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SpawnerController : MonoBehaviour {


    public float interval = 30.0f;
    public float countdownToSpawn = 3.0f;
    private bool isActivated = false;


	// Use this for initialization
	void Start () {
        // TODO: Get spawners


        InvokeRepeating("IncreaseEnemySpawn", interval + countdownToSpawn, interval);


    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > countdownToSpawn && !isActivated) {
            isActivated = true;
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
        }
    }

    void IncreaseEnemySpawn() {
        foreach (Transform child in transform)
        {

            EnemySpawner enemySpawner = child.gameObject.GetComponent<EnemySpawner>();

            enemySpawner.spawnCooldown = enemySpawner.spawnCooldown * 0.7f;

        }

    }

}
