﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldController : MonoBehaviour {

	private bool isShield = false;
    private int shieldPoints = 0;
    private float shieldActivatedAt = 0;
    public Sprite fusionBrain;
    public Sprite regularBrain;
	public GameObject backgroundImage;
	public GameObject shieldSound;
    public GameObject soundTrack;

    public GameObject AntiPlayer;


    private Color origAntiPlayerColor;
    private Color origPlayerColor;

    private bool isShieldReady = false;

    public void AddShieldPoints(int points) {
        shieldPoints += points;
    }

    public int GetShieldPoints() {
        return shieldPoints;
    }


    // Use this for initialization
    void Start () {

        origAntiPlayerColor = AntiPlayer.GetComponent<Image>().color;
        origPlayerColor = GetComponent<Image>().color;

    }
	
	// Update is called once per frame
	void Update () {

        if (GetShieldPoints() > CONSTANTS.SHIELD_SCORE_THRESHOLD && !isShieldReady)
        {
            isShieldReady = true;
            AntiPlayer.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
            GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f);
            // indicate can activate shield
        } else if (GetShieldPoints() < CONSTANTS.SHIELD_SCORE_THRESHOLD && isShieldReady) {
            isShieldReady = false;

            AntiPlayer.GetComponent<Image>().color = origAntiPlayerColor;
            GetComponent<Image>().color = origPlayerColor;
        }

        if (isShield) {
            AntiPlayer.SetActive(false);
        } else {

            AntiPlayer.SetActive(true);
        }

        if (isShield && Time.time - shieldActivatedAt > CONSTANTS.SHIELD_DURATION)
        {
            DisableShield();
        }
    }


    private void DisableShield()
    {
        isShield = false;
        GetComponent<Image>().sprite = regularBrain;
		backgroundImage.GetComponent<LerpColor> ().enabled = false;
        soundTrack.GetComponent<AudioSource>().mute = false;
    }

    private void EnableShield()
    {
		backgroundImage.GetComponent<LerpColor> ().enabled = true;
        isShield = true;
        shieldActivatedAt = Time.time;
        shieldPoints = 0;
        GetComponent<Image>().sprite = fusionBrain;
        GetComponent<RotateBrain>().speed = 480;
        GetComponent<RotateBrain>().removeSpeedEverySecond = 60;
		shieldSound.GetComponent<AudioSource> ().Play ();
        soundTrack.GetComponent<AudioSource>().mute = true;
    }


    public bool GetIsShield()
    {
        return isShield;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "AntiPlayer")
        {
            if (shieldPoints >= CONSTANTS.SHIELD_SCORE_THRESHOLD)
            {
                EnableShield();
            }
        }
    }
}
