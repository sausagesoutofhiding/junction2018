﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowEnemy : MonoBehaviour {

	public int  followSpeed;
	public GameObject target;

	void Start() {
		target = GameObject.FindWithTag ("Player");
	}


	void Update(){
		if (target) {
			transform.LookAt (target.transform.position);
			transform.Rotate (new Vector3 (0, -90, 0), Space.Self);//correcting the original rotation

			if (Vector3.Distance (transform.position, target.transform.position) > 1f) {//move if distance from target is greater than 1
				transform.Translate (new Vector3 (followSpeed * Time.deltaTime, 0, 0));
			}
		}
	}
}
