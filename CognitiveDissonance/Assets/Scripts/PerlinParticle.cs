﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinParticle : MonoBehaviour {

    // Add velocity and direction
    // Modify direction on each update with perlin noise.

    //particle.rigidbody2D.velocity

    private Rigidbody2D rigi;
    private float perlinX;
    private float magnitude;

    private float speed;


    public void Init(Vector2 dir, float _speed){

        perlinX = Random.Range(0.0f, 100f);
        speed = _speed;


        //particleSpeed = new Vector2(Random.Range(100.0f, 300.0f), Random.Range(100.0f, 300.0f));
        rigi = GetComponent<Rigidbody2D>();
        rigi.velocity = dir * 300.0f;

        magnitude = rigi.velocity.magnitude;


    }

    // Update is called once per frame
    void Update ()
    {
        float noiseVal = (Mathf.PerlinNoise(perlinX, Time.time) - 0.5f) * 100;  // between - 0.5 and 0.5



        Vector2 newVector = new Vector2(rigi.velocity.x + noiseVal, rigi.velocity.y + noiseVal);


        Vector2 clampedVector = Vector2.ClampMagnitude(newVector, magnitude);


        rigi.velocity = clampedVector;




    }
}
