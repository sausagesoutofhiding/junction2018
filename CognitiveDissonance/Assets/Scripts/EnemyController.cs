﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	public GameObject destroyAnimation;
    private GameObject scoreKeeper;
    private ShieldController shieldController;
    public int scoreReward;
    public int shieldReward;
	public GameObject GameOverText;

    private void Start()
    {
		GameOverText = GameObject.FindWithTag ("GameOverText");
        scoreKeeper = GameObject.FindWithTag("GameCanvas");

        GameObject go = GameObject.FindWithTag("Player");
        if (go) {
            shieldController = go.GetComponent<ShieldController>();
        }
    }

    // Destroy bullet and object on collision
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlayerBullet")
        {
			StartCoroutine( DestroyEnemy());
            shieldController.AddShieldPoints(shieldReward);

            Destroy(col.gameObject);

            if (destroyAnimation != null)
            {
                Instantiate(destroyAnimation, col.transform.position, col.transform.rotation, null);
            }

        } else if (
            col.gameObject.tag == "Player"
        ) {

            bool isShield = col.gameObject.GetComponent<ShieldController>().GetIsShield();
            int lifeCount = col.gameObject.GetComponent<PlayerController>().GetLives();

            if (isShield)
            {
				StartCoroutine( DestroyEnemy());

            } else if (lifeCount > 1) {

                StartCoroutine(DestroyEnemy());

                col.gameObject.GetComponent<PlayerController>().RemoveLife();

            } else {

                StartCoroutine(EndGame());

                // TODO: GAMEOVER
                scoreKeeper.GetComponent<ScoreHandler>().SetHighScore();

                Destroy(col.gameObject);
                gameObject.transform.position = new Vector2(99999999999999999, 999999999999999999);
            }


        }
    }

	IEnumerator DestroyEnemy() {


        CircleCollider2D circleCollider = GetComponent<CircleCollider2D>();

        if (circleCollider) {
            circleCollider.enabled = false;
        }

        BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();

        if (boxCollider)
        {
            boxCollider.enabled = false;
        }




        if (GetComponent<AudioSource> () != null) {
			GetComponent<AudioSource> ().Play ();
		}
		scoreKeeper.GetComponent<ScoreHandler>().AddToScore(scoreReward);
		Destroy(GetComponent<Rigidbody2D>());
		gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x * 0.5f, gameObject.transform.localScale.y * 0.5f, gameObject.transform.localScale.z * 0.5f);
		yield return new WaitForSeconds (0.05f);
		gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x * 0.5f, gameObject.transform.localScale.y * 0.5f, gameObject.transform.localScale.z * 0.5f);
		yield return new WaitForSeconds (0.05f);
		gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x * 0.5f, gameObject.transform.localScale.y * 0.5f, gameObject.transform.localScale.z * 0.5f);
		yield return new WaitForSeconds (0.05f);
		gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x * 0.5f, gameObject.transform.localScale.y * 0.5f, gameObject.transform.localScale.z * 0.5f);
		yield return new WaitForSeconds (0.05f);
		Destroy(this.gameObject);
	}

		

    IEnumerator EndGame() {
		if (GetComponent<AudioSource> () != null) {
			GetComponent<AudioSource> ().pitch = 0.3f;
			GetComponent<AudioSource> ().Play ();
		}
		GameOverText.GetComponent<Text>().enabled = true;
        yield return new WaitForSeconds(3);

        GameObject.FindWithTag("Menu").GetComponent<MainMenuHandler>().ExitGame(false);
    }
}
