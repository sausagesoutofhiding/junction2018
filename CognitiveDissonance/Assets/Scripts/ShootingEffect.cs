﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingEffect : MonoBehaviour {

    public GameObject particleGO;
    private GameObject gameCanvas;
    public float particleSpeed =  100.0f;

    // Use this for initialization
    void Start () {
        gameCanvas = GameObject.FindWithTag("GameCanvas");
	}

    public void Trigger(Vector2 dir, float speed) {
        for (int i = 0; i < 5; i++)
        {
            SpawnParticle(dir, speed);

        }
    }

    void SpawnParticle(Vector2 dir, float speed) {
        GameObject particle = Instantiate(particleGO, gameCanvas.transform, true);
        particle.GetComponent<PerlinParticle>().Init(dir, speed);
        particle.transform.position = transform.position;
    }
}
