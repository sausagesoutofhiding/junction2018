﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogScript : MonoBehaviour {

    public string[] DialogList = new string[10];
    int currentString = -1;
    int currentLetter;
    string stringSoFar;
    public float TimeBetweenLetters;
    public float TimeBetweenWords;
    public int AmountOfDialog;
    IEnumerator co;
    bool WeDoingThis;
	public GameObject mainGameMenu;

	// Use this for initialization
	void Start () {
        co = ChangingTheDialogue();
        StartCoroutine(co);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown) {
            if (co != null)
            {
                StopCoroutine(co);
            }
            co = ChangingTheDialogue();
            StartCoroutine(co);
        }
	}

    IEnumerator ChangingTheDialogue()
    {

        stringSoFar = "";
        currentString++;
        currentLetter = -1;
        WeDoingThis = true;
        if (AmountOfDialog > currentString)
        {
            do
            {
                currentLetter++;
                if (DialogList[currentString][currentLetter].ToString() == "1")
                {

                }
                else if (DialogList[currentString][currentLetter].ToString() == "2")
                {

                }
                else if (DialogList[currentString][currentLetter].ToString() == "3")
                {

                }

                else
                {

                    if (DialogList[currentString][currentLetter].ToString() == " ")
                    {
                        yield return new WaitForSeconds(TimeBetweenWords);
                        stringSoFar += DialogList[currentString][currentLetter];
                        gameObject.GetComponent<Text>().text = stringSoFar;
                    }
                    else
                    {
                        stringSoFar += DialogList[currentString][currentLetter];
                        gameObject.GetComponent<Text>().text = stringSoFar;
                    }



                    yield return new WaitForSeconds(TimeBetweenLetters);
                }
                if (DialogList[currentString].Length == currentLetter + 1)
                {
                    WeDoingThis = false;
                }

        } while (WeDoingThis) ;
    }
        else
        {
			mainGameMenu.GetComponent<MainMenuHandler> ().LoadScene (2);
		}
    }
}
